package lenguaje;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author Eri Fernández
 */
public class Lenguaje {
	/**
	 * @param args argumentos de la línea de comandos Recibe dos argumentos, el
	 *             primero, un número entero que indica el número de palabras a
	 *             generar. El segundo parámetro la ruta del fichero donde se
	 *             escribirán las palabras.
	 */
	public static void main(String[] args) {
		int n = 10;// número palabras a generar por cada instancia por defecto
		String path = new File("lenguaje.txt").getAbsolutePath(); // ruta por defecto del archivo generado

		switch (args.length) {
		case 1:
			n = Integer.parseInt(args[0]);
			break;
		case 2:
			n = Integer.parseInt(args[0]);
			path = args[1];
			break;
		default:
			break;
		}

		for (int i = 0; i < n; i++) {
			escribir(generateWord(), path);
		}
	}

	public static char[] generateWord() {

		Random rlw = new Random();

		// definimos la longitud de la siguiente palabra
		int lengthWord = rlw.nextInt(15) + 2;

		char[] word = new char[lengthWord];

		for (int x = 0; x < lengthWord; x++) {

			Random r = new Random();
			char c = (char) (r.nextInt(26) + 'a');

			if (Math.random() < 0.5) {
				char upperC = Character.toUpperCase(c);
				word[x] = upperC;
			} else {
				word[x] = c;
			}
		}
		return word;
	}

	public static void escribir(char[] c, String path) {

		char[] word = c;

		try {
			FileWriter fw = new FileWriter(path, true);
			/*
			 * segundo parámetro: true, para escribir a continuación sin él, sobreescribimos
			 * el mensaje
			 */

			BufferedWriter miBuffer = new BufferedWriter(fw);

			miBuffer.write(word, 0, word.length);
			miBuffer.newLine();

			miBuffer.close();
			fw.close();

		} catch (IOException e) {
			System.out.println("Archivo inaccesible");
		}
	}
}

package ordenarNumeros;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 
 * @author Eri Fernández
 */

public class OrdenarNumeros {

	public static void main(String[] args) {
		
		int[] numeros = new int[40];

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Introduce " + numeros.length + " números enteros: ");
		
		try {
			for (int i = 0; i < numeros.length; i++) {
				numeros[i] = scan.nextInt();
			}
		}catch(Exception e) {
			System.err.println("Error: el valor introducido no es un número entero");
		}finally {
			scan.close();
		}
				
		Arrays.sort(numeros);

		for (int i : numeros) {
			System.out.println("Numeros del array: " + i);
		}
	}
}
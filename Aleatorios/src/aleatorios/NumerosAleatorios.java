package aleatorios;

/**
 * 
 * @author Eri Fernández
 */

public class NumerosAleatorios {

	public static void main(String[] args) {
		
		int cantidad = 40;
		int num;
		
		// Generar 40 números aleatorios e imprimirlos por consola
		for (int i = 0; i < cantidad; i++) {
			num = (int) (Math.random() * 100);
			System.out.println(num); 
		}

	}

}

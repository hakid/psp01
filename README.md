# ENTREGA PSP01

Consta de cuatro apps relacionadas dos a dos:

1. Ejercicio 1

    * Aleatorios: genera 40 números aleatorios entre los valores 0 y 100.
    * OrdenarNumeros: ordena de menor a mayor una lista de números.

2. Ejercicio 2

    * Lenguaje: genera una lista de conjuntos de caracteres aleatórios, cada uno de ello en una línea, y los guarda en un fichero.
    * Colaborar: llama a la app lenguaje un número de veces para que se generen más palabras y se guarden en un fichero.

***

## Pasos generales para creación de archivos .jar

Seleccionaremos el menu *File* y en él, la opción *Export*, como se se en la siguiente imagen.

![Exportar proyecto][exportar]

A continuación elegiremos en la carpeta *Java*, la opción *Runnable JAR File*

![Seleccionar jar ejecutable][jarEjecutable]

Por último seleccionaremos la configuración del proyecto que deseamos exportar y le indicamos una ruta y un nombre.

![Crear jar file][crearJar]

Para ejecutar los archivos mediante la consola tendremos que estar situados en el directorio donde se encuentra la app o indicar la ruta completa.

***

## Uso de las apps

### Ejercicio 1

Probamos a generar **números aleatorios** ejecutando aleatorios.jar

Para ello ejecutamos el siguiente código en la consola `java -jar aleatorios.jar`. 

![Ejecutar aleatorios.jar][aleatorios]

Ahora generamos **números aleatorios y los ordenamos** con ordenarNumeros.jar. Pasamos la salida de la primera app a la entrada de la segunda mediante una tubería. Para ello utilizaremos `java -jar aleatorios.jar | java -jar ordenarNumeros.jar`

![Tuberia aleatorios.jar ordenarNumeros.jar][tubería]

### Ejercicio 2

Consta de dos apps, una que genera palabras y otra que crea instancias de la primera para que colaboren en llenar un fichero.

#### lenguaje.jar

Tenemos **3 opciones** a la hora de ejecutar la app.

1. No pasar ningún argumento

```
java -jar lenguaje.jar
```
2. Pasar 1 argumento que indica el número de palabras a generar 


```
java -jar lenguaje.jar 20
```
3. Pasr 2 argumentos, donde el segundo indica la ruta del archivo donde se escribirán las palabras. Si el archivo no existe se creará. 

```
java -jar lenguaje.jar 15 pruebaLenguaje.txt
``` 
> Si solo se especifica el nombre del archivo contenedor, este se creará en la ruta desde la que se ejecuta el programa.

**Datos por defecto**

> Si no se indica el número de palabras, por defecto se generarán 10.

> Si no se especifica ruta del archivo contenedor, se creará un archivo llamado lenguaje.txt, en la ruta desde la que se ejecuta el programa.



#### colaborar.jar

Tenemos **5 opciones** a la hora de ejecutar la app.

1. No pasar ningún argumento.

```
java -jar colaborar.jar
```

2. Pasar 1 argumento, que indicará la ruta y el nombre del programa que queremos ejecutar desde la app.

```
java -jar colaborar.jar lenguaje.jar
```
> Si solo indicamos el nombre, lo buscará en la misma ruta que desde donde se ejecuta colaborar.jar

3. Pasar 2 argumentos, el segundo indica el número de veces que se quiere ejecutar el programa

```
java -jar colaborar.jar lenguaje.jar 30 
```

4. Pasar 3 argumentos, el tercero indica el número de palabras que generará y escribirá lenguaje.jar

```
java -jar colaborar.jar lenguaje.jar 30 15 
```

5. Pasar 4 argumentos, el cuarto indica la ruta y el nombre del archivo donde se guardarán las palabras generadas.

```
java -jar colaborar.jar lenguaje.jar 30 15 colaborar.txt
```

> Si solo indicamos el nombre, lo buscará en la misma ruta que desde donde se ejecuta colaborar.jar, y si no existe se creará.

**Datos por defecto**

>Si no se especifica la ruta y el nombre del programa que queremos ejecutar desde la app, se

> Si no se indica el número de veces que se debe ejecutar el programa, se abrirán 10 procesos de lenguaje.jar.

> Si no se indica el número de palabras a escribir por cada proceso, por defecto se generarán 10.

> Si no se especifica ruta del archivo contenedor, se creará un archivo llamado colaborar.txt, en la ruta desde la que se ejecuta el programa.

***

## Licencia
[MIT](https://choosealicense.com/licenses/mit/)

[exportar]: /img/Export.jpeg "Exportar proyecto."
[jarEjecutable]: /img/jar.jpeg "Seleccionar jar ejecutable."
[crearJar]: /img/config_name.jpeg "Crear archivo jar en ruta seleccionada."
[aleatorios]: /img/cmd_aleatorios.jpeg "Ejecutar aleatorios.jar."
[tubería]: /img/cmd_tuberia.jpeg "Ejecutar aleatorios.jar y ordenarNumeros.jar, y conectar la salida del primero a la entrada del segundo."




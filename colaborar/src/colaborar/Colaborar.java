package colaborar;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Colaborar {
	/*
	 * @param args argumentos de la línea de comandos Recibe cuatro argumentos, el
	 * primero, un String indicando la ruta del ejecutable. El segundo, un número
	 * entero que indica el número de veces que se llama al ejecutable. El tercero,
	 * un número entero que indica el número de palabras a generar. El cuarto
	 * parámetro la ruta del fichero donde se escribirán las palabras.
	 */
	public static void main(String[] args) {

		int n = 10; // número de instancioas de lenguaje por defecto
		int nWords = 10; // número palabras a generar por cada instancia por defecto
		String appPath = System.getProperty("user.dir") + "\\lenguaje.jar"; // posible si está en el mismo directorio
		String filePath = new File("lenguaje.txt").getAbsolutePath(); // ruta por defecto del archivo generado

		switch (args.length) {
		case 1:
			appPath = args[0];
			break;
		case 2:
			appPath = args[0];
			n = Integer.parseInt(args[1]);
			break;
		case 3:
			appPath = args[0];
			n = Integer.parseInt(args[1]);
			nWords = Integer.parseInt(args[2]);
			break;
		case 4:
			appPath = args[0];
			n = Integer.parseInt(args[1]);
			nWords = Integer.parseInt(args[2]);
			filePath = args[3];
			break;
		default:
			break;
		}

		if (Files.notExists(Paths.get(appPath))) {
			System.err.println("No se encuentra el archivo");
			return;
		}

		String txtTotalWords;

		for (int i = 1; i <= n; i++) {
			int totalWords = nWords * i;
			txtTotalWords = String.valueOf(totalWords);
			try {
				Process p = Runtime.getRuntime().exec("java -jar " + appPath + " " + txtTotalWords + " " + filePath);
				System.out.println("Se ejecuta");
				System.err.println("Intentantdo acceder a: " + appPath);
			} catch (IOException e) {
				System.err.println("Problema al intentar ejecutar lenguaje.jar");
				e.printStackTrace();
			}
		}

	}

}
